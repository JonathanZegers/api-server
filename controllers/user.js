const User = require('../models/User');

exports.getAllUsers = (req, res) => {
  User.find((err, allUsers) => {
    if (err) {
      console.log(`index error: ${err}`);
    } else {
      res.json({
        users: allUsers,
      });
    }
  });
};

exports.getUserById = (req, res) => {
  User.findOne(
    {
      _id: req.params.id,
    },
    (err, user) => {
      if (err) {
        console.log(`show error: ${err}`);
      }
      res.json(user);
    }
  );
};

exports.createUser = (req, res) => {
  const newUser = new User(req.body);
  newUser.save((err, user) => {
    if (err) {
      console.log(`save error: ${err}`);
    }
    console.log(`saved new user: ${user}`);
    return res.json(user);
  });
};

exports.updateUser = (req, res) => {
  const userId = req.params.id;
  console.log(req.body);
  User.findOne(
    {
      _id: userId,
    },
    (err, foundUser) => {
      if (err) {
        console.log('could not find user');
      }
      foundUser.name.firstName =
        req.body.name.firstName || foundUser.name.firstName;
      foundUser.name.lastName =
        req.body.name.lastName || foundUser.name.lastName;
      foundUser.email = req.body.email || foundUser.email;
      foundUser.age = req.body.age || foundUser.age;
      foundUser.isAdmin = req.body.isAdmin || foundUser.isAdmin;
      foundUser.updated = Date.now();
      console.log(`updated: ${foundUser.email}`);
      foundUser.save((err, user) => {
        if (err) {
          console.log(`update error: ${err}`);
        }
        console.log(`updated: ${user.email}`);
        res.json(user);
      });
    }
  );
};

exports.deleteUser = (req, res) => {
  const userId = req.params.id;
  User.findOneAndDelete({
    _id: userId,
  })
    .populate('user')
    .exec((err) => {
      if (err) {
        console.log(err);
        res.json({ error: 'user delete error' });
      } else {
        res.json({ message: 'user deleted successfully!' });
      }
    });
};
