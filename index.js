// Import files 
const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const dotenv = require('dotenv');
const models = require('./models');

// Setup app configuration to use express, .env, and body-parser
const app = express();
dotenv.config();
app.use(bodyParser.json());

// Routes setup
const userRoutes = require('./routes/user');
app.use('/', userRoutes);

// Startup database connection then server
connectDB().then(async () => {
  app.listen(process.env.PORT, () => {
    console.log(`you api server is running on localhost:${process.env.PORT}`);
  });
});
