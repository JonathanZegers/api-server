const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
  //list model properties
  name: { firstName: String, lastName: String },
  email: { type: String, required: true },
  isAdmin: { type: Boolean, default: false },
  age: Number,
  updated: Date,
  created: { type: Date, default: Date.now },
});

const User = mongoose.model('User', UserSchema);

module.exports = User;
