const mongoose = require('mongoose');
const User = require('./User');

mongoose.set('useUnifiedTopology', true);

connectDB = () => {
  return mongoose
    .connect(process.env.MONGO_URI, { useNewUrlParser: true })
    .then(() => console.log('DB Connected!'));
};

const models = { User };
